using System;
using NUnit.Framework;
using Oop.Shapes.Factories;

namespace Oop.Shapes.Tests
{
	[TestFixture]
	public class CircleTests
	{
		private ShapeFactory _shapeFactory;

		[OneTimeSetUp]
		public void FixtureSrtUp()
		{
			_shapeFactory = new ShapeFactory();
		}

		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(int.MinValue)]
		public void Constructor_InvalidRadius_ThrowsArgumentOutOfRangeException(int radius)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				Shape _ = _shapeFactory.CreateCircle(radius);
			});
		}

		[TestCase(1, ExpectedResult = 3.1416)]
		[TestCase(5, ExpectedResult = 78.5398)]
		[TestCase(12, ExpectedResult = 452.3893)]
		public decimal Area___ReturnsExpectedValue(int radius)
		{
			Shape circle = _shapeFactory.CreateCircle(radius);
			var area = circle.Area;

			return (decimal)Math.Round(area, 4);
		}

		[TestCase(1, ExpectedResult = 6.2832)]
		[TestCase(5, ExpectedResult = 31.4159)]
		[TestCase(12, ExpectedResult = 75.3982)]
		public decimal Perimeter___ReturnsExpectedValue(int radius)
		{
			Shape circle = _shapeFactory.CreateCircle(radius);
			var perimeter = circle.Perimeter;

			return (decimal)Math.Round(perimeter, 4);
		}

		[Test]
		public void VertexCount___Returns0()
		{
			Shape circle = _shapeFactory.CreateCircle(1);
			var vertexCount = circle.VertexCount;

			Assert.AreEqual(0, vertexCount);
		}

		[TestCase(4, ExpectedResult = true)]
		[TestCase(3, ExpectedResult = false)]
		[TestCase(5, ExpectedResult = false)]
		public bool IsEqual_OtherCircle_ReturnsExpectedResult(int a)
		{
			var circle1 = _shapeFactory.CreateCircle(4);
			var circle2 = _shapeFactory.CreateCircle(a);

			return circle1.IsEqual(circle2);
		}

		[Test]
		public void IsEqual_Triangle_ReturnsFalse()
		{
			var circle = _shapeFactory.CreateCircle(4);
			var triangle = _shapeFactory.CreateTriangle(4, 5, 6);

			var isEqual = circle.IsEqual(triangle);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Square_ReturnsFalse()
		{
			var circle = _shapeFactory.CreateCircle(4);
			var square = _shapeFactory.CreateSquare(4);

			var isEqual = circle.IsEqual(square);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Rectangle_ReturnsFalse()
		{
			var circle = _shapeFactory.CreateCircle(4);
			var rectangle = _shapeFactory.CreateRectangle(4, 5);

			var isEqual = circle.IsEqual(rectangle);

			Assert.False(isEqual);
		}
	}
}
